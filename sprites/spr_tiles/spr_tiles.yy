{
    "id": "8b8fa980-7d1a-471c-9af0-20553a128a15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 933,
    "bbox_left": 0,
    "bbox_right": 911,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35ec71f9-3945-455c-ae6a-d7903bb970e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa980-7d1a-471c-9af0-20553a128a15",
            "compositeImage": {
                "id": "e65db114-2f6f-4a37-87fd-010fe1058bf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ec71f9-3945-455c-ae6a-d7903bb970e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a2b97f-6835-4b15-97f8-01fdde683712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ec71f9-3945-455c-ae6a-d7903bb970e9",
                    "LayerId": "00b6ebfc-408e-41d6-bf21-dc56d2699f98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 936,
    "layers": [
        {
            "id": "00b6ebfc-408e-41d6-bf21-dc56d2699f98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b8fa980-7d1a-471c-9af0-20553a128a15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 914,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "abd4fe30-1b54-4ff9-8325-7631d2d80001",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_panel_pause",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61beb519-6a44-440d-92dd-9d7896455a56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abd4fe30-1b54-4ff9-8325-7631d2d80001",
            "compositeImage": {
                "id": "6b49e3ee-3cd1-474e-97ec-57c35ccee482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61beb519-6a44-440d-92dd-9d7896455a56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09b60059-84de-4daa-8942-1dccb077b6b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61beb519-6a44-440d-92dd-9d7896455a56",
                    "LayerId": "a0e9eed2-6ecc-437f-8bcc-087a48d96367"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "a0e9eed2-6ecc-437f-8bcc-087a48d96367",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abd4fe30-1b54-4ff9-8325-7631d2d80001",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 150
}
{
    "id": "6504a633-dfb6-4893-ad3c-6189d7d6f8cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pause",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 11,
    "bbox_right": 38,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "765b27a8-d66a-4f72-a24c-e3b2a727c93e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6504a633-dfb6-4893-ad3c-6189d7d6f8cd",
            "compositeImage": {
                "id": "c1724008-b2f3-4fc3-9f24-5a01f74de28d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "765b27a8-d66a-4f72-a24c-e3b2a727c93e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22ef95f3-62ef-4f53-a2b3-27e8c2babc00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "765b27a8-d66a-4f72-a24c-e3b2a727c93e",
                    "LayerId": "e79fe41b-faf5-4205-8c29-e765d07f78c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "e79fe41b-faf5-4205-8c29-e765d07f78c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6504a633-dfb6-4893-ad3c-6189d7d6f8cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 2,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 49,
    "yorig": 0
}
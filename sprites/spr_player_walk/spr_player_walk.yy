{
    "id": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f6b3aea-40d1-4256-b3eb-2a469759a08e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "c76bfb12-a5c8-41c7-91a9-01b802c4a53e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f6b3aea-40d1-4256-b3eb-2a469759a08e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a47884-f82b-45e5-8b7a-753364e22b0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f6b3aea-40d1-4256-b3eb-2a469759a08e",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        },
        {
            "id": "78263cc0-8125-4f7d-be71-18631ea8bf9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "ab0ad595-849e-4664-928d-71336edc2f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78263cc0-8125-4f7d-be71-18631ea8bf9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3397a086-3d7d-4fd1-a23f-3141e5d270c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78263cc0-8125-4f7d-be71-18631ea8bf9e",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        },
        {
            "id": "6b1d7e6f-10d3-442a-9c2d-76955bb61ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "082e518f-ff97-4684-8b94-bae567c42798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b1d7e6f-10d3-442a-9c2d-76955bb61ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f51962-e2a1-49d7-a5f5-5253ec1f7abd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b1d7e6f-10d3-442a-9c2d-76955bb61ed2",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        },
        {
            "id": "cf94943d-c761-484d-a2bf-53214ccd7ffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "24409dbc-c006-4e98-9d7d-b39cc8cd3a98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf94943d-c761-484d-a2bf-53214ccd7ffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "711d36c6-e56d-483f-a42e-f9d7a6fc0241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf94943d-c761-484d-a2bf-53214ccd7ffb",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        },
        {
            "id": "726ac7ea-cf35-4bff-a5eb-f4b70e67728a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "c903f35d-d3eb-468c-9685-d02a10600f3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "726ac7ea-cf35-4bff-a5eb-f4b70e67728a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de2e591a-d776-4632-bc12-9dc577329dd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "726ac7ea-cf35-4bff-a5eb-f4b70e67728a",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        },
        {
            "id": "bdbe8581-accc-4bb1-bc06-d738669db38a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "9e6b0643-a4b3-4753-bbea-a85d062dcde0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdbe8581-accc-4bb1-bc06-d738669db38a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf52c9be-44be-4b32-93ea-918a4cc78197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdbe8581-accc-4bb1-bc06-d738669db38a",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        },
        {
            "id": "f118d0f4-deda-4479-aa23-eb3cf9705685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "49a9a2fa-05a1-459f-b4d8-730132e78421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f118d0f4-deda-4479-aa23-eb3cf9705685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69ea799-ae2a-4cb9-bc78-5f3af9191a9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f118d0f4-deda-4479-aa23-eb3cf9705685",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        },
        {
            "id": "7284fa19-6f03-4359-8abd-615f66a70967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "cdba46c7-35a3-4dc3-ae87-7cec2b47c461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7284fa19-6f03-4359-8abd-615f66a70967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f917e7c1-3482-4249-ae91-814fcfa72d3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7284fa19-6f03-4359-8abd-615f66a70967",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        },
        {
            "id": "256d5136-6fab-4489-80cc-1582bbfb4c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "c19e6d15-af79-4ae4-91b7-c3a9b07272d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "256d5136-6fab-4489-80cc-1582bbfb4c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad98494-14f5-4900-9201-847b3a6e99e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "256d5136-6fab-4489-80cc-1582bbfb4c76",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        },
        {
            "id": "01500b9a-8119-4497-85bc-d11d001e75ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "e5113adf-ed4e-4ccd-83a3-c54762a596a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01500b9a-8119-4497-85bc-d11d001e75ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed75b055-3d51-4287-8b2c-a9959ab1073b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01500b9a-8119-4497-85bc-d11d001e75ed",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        },
        {
            "id": "448e7d44-240f-45f4-8ab5-755534cc3d89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "compositeImage": {
                "id": "09aca950-bf6d-40ff-9870-e17e5edee3e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "448e7d44-240f-45f4-8ab5-755534cc3d89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aaee265-cd99-47ee-b253-a5c4dc35b6cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "448e7d44-240f-45f4-8ab5-755534cc3d89",
                    "LayerId": "08ce76a5-a9ab-451e-b989-cac1c1be33bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 97,
    "layers": [
        {
            "id": "08ce76a5-a9ab-451e-b989-cac1c1be33bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae93c786-7cd9-43f4-9a51-e821dea344e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 36,
    "yorig": 96
}
{
    "id": "bdc4bb51-b652-4234-a0c8-e16061c1467f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_return",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 189,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09b8c9f9-c582-4e33-80a7-34b432e39182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdc4bb51-b652-4234-a0c8-e16061c1467f",
            "compositeImage": {
                "id": "b600da5b-5421-441e-bf13-d4c172a68d80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09b8c9f9-c582-4e33-80a7-34b432e39182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a26a602b-2cbd-41f7-8183-ea056e3b9fdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09b8c9f9-c582-4e33-80a7-34b432e39182",
                    "LayerId": "154b6fb3-db4b-4eb7-af23-e9037234c506"
                }
            ]
        },
        {
            "id": "2bc63ac2-f5ae-43e6-9044-38dffb63bdd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdc4bb51-b652-4234-a0c8-e16061c1467f",
            "compositeImage": {
                "id": "89e2ed0d-e0b1-49f7-b62b-46ccd6a4fa75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc63ac2-f5ae-43e6-9044-38dffb63bdd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bae86dec-404f-42f0-bf92-40694378ceae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc63ac2-f5ae-43e6-9044-38dffb63bdd2",
                    "LayerId": "154b6fb3-db4b-4eb7-af23-e9037234c506"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "154b6fb3-db4b-4eb7-af23-e9037234c506",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdc4bb51-b652-4234-a0c8-e16061c1467f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 190,
    "xorig": 95,
    "yorig": 24
}
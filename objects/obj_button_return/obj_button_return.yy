{
    "id": "ce59dce4-dfd7-49f5-93c8-516c570c42a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_return",
    "eventList": [
        {
            "id": "9de886c7-a1d5-4c28-934d-dae5ad3f059f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce59dce4-dfd7-49f5-93c8-516c570c42a6"
        },
        {
            "id": "f5daa0e3-f9cc-4dd1-8f55-8d4b201d9dfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ce59dce4-dfd7-49f5-93c8-516c570c42a6"
        },
        {
            "id": "23dc006f-1144-42f2-b9be-16d2c6458340",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ce59dce4-dfd7-49f5-93c8-516c570c42a6"
        },
        {
            "id": "43df159c-6585-4f3e-94d2-266d81e53528",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "ce59dce4-dfd7-49f5-93c8-516c570c42a6"
        },
        {
            "id": "45fd440a-dbf3-40be-8833-aa712fe8388a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "ce59dce4-dfd7-49f5-93c8-516c570c42a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bdc4bb51-b652-4234-a0c8-e16061c1467f",
    "visible": true
}